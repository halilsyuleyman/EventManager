﻿using EventManager.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EventManager.DataAccess
{
    public class EventManagerContext : DbContext
    {
        public EventManagerContext() : base("EventManagerDb")
        {

        }

        public DbSet<Event> Events { get; set; }
    }
}
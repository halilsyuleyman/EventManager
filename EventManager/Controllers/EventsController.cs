﻿using EventManager.DataAccess;
using EventManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventManager.Controllers
{
    public class EventsController : Controller
    {
        // GET: Students
        public ActionResult Index()
        {
            EventRepository repo = new EventRepository();
            List<Event> model = repo.GetAll();

            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Event model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            EventRepository repo = new EventRepository();
            repo.Insert(model);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            EventRepository repo = new EventRepository();
            Event model = repo.Get(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Event model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            EventRepository repo = new EventRepository();
            repo.Update(model);

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            EventRepository repo = new EventRepository();
            Event model = repo.Get(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(Event model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            EventRepository repo = new EventRepository();
            model = repo.Get(model.Id);

            repo.Delete(model);

            return RedirectToAction("Index");
        }
    }
}